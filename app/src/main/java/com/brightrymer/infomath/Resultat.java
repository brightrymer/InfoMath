package com.brightrymer.infomath;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bright on 19/11/2015.
 * For InfoMath18
 */
public class Resultat {

    // Equation
    private Double discriminant;
    private Double x1;
    private Double x2;
    private Double x3;
    private String type;

    // Stat
    private Double moyenne;
    private Double ecartType;
    private String mode;
    private Double mediane;

    // Matrice
    private List<Double> matResultat = new ArrayList<>();
    private String matType;


    public Resultat(String myTyp, Double myDiscr, Double myX1, Double myX2) { // Second Degree

        if (!myTyp.isEmpty() && myDiscr != null) {

            this.setType(myTyp);
            this.setDiscriminant(myDiscr);

            if ((myTyp.equals("Sec Deg 2") || myTyp.equals("Sec Deg 0")) && myX1 != null && myX2 != null) {

                this.setX1(myX1);
                this.setX2(myX2);
            } else {
                if (myTyp.equals("Sec Deg 1") && myX1 != null) {

                    this.setX1(myX1);
                }
            }
        }
    }

    public Resultat(String myTyp, Double myDiscr, Double myX1, Double myX2, Double myX3) { // Third Degree

        if ((myTyp.equals("Thir Deg 3") || myTyp.equals("Thir Deg 3.1") || myTyp.equals("Thir Deg 3.2")) && myDiscr != null && myX1 != null && myX2 != null) { // Thir Deg 3.1 représente trois solutions dont deux complexes.

            this.setType(myTyp);
            this.setDiscriminant(myDiscr);
            this.setX1(myX1);
            this.setX2(myX2);
            this.setX3(myX3);
        } else {
            if (myTyp.equals("Thir Deg 2") && myDiscr != null && myX1 != null && myX2 != null) {
                this.setType(myTyp);
                this.setDiscriminant(myDiscr);
                this.setX1(myX1);
                this.setX2(myX2);
            }
        }
    }

    public Resultat(Double moy, Double ecart, String mod, Double median) {

        if (moy != null && ecart != null && mod != null && median != null) {

            this.setMoyenne(moy);
            this.setEcartType(ecart);
            this.setMode(mod);
            this.setMediane(median);
        }
    }

    public Resultat(List<Double> matA, int size) {

        if (size == 4) {

            this.setMatType("Mat2");
        }

        if (size == 9) {

            this.setMatType("Mat3");
        }
        this.setMatResultat(matA);
    }

    //Accessors

    public Double getDiscriminant() {
        return discriminant;
    }

    public void setDiscriminant(Double discriminant) {
        this.discriminant = discriminant;
    }

    public Double getX1() {
        return x1;
    }

    public void setX1(Double x1) {
        this.x1 = x1;
    }

    public Double getX2() {
        return x2;
    }

    public void setX2(Double x2) {
        this.x2 = x2;
    }

    public Double getX3() {
        return x3;
    }

    public void setX3(Double x3) {
        this.x3 = x3;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getMoyenne() {
        return moyenne;
    }

    public void setMoyenne(Double moyenne) {
        this.moyenne = moyenne;
    }

    public Double getEcartType() {
        return ecartType;
    }

    public void setEcartType(Double ecartType) {
        this.ecartType = ecartType;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public Double getMediane() {
        return mediane;
    }

    public void setMediane(Double mediane) {
        this.mediane = mediane;
    }

    public List<Double> getMatResultat() {
        return matResultat;
    }

    public void setMatResultat(List<Double> matResultat) {
        this.matResultat = matResultat;
    }

    public String getMatType() {
        return matType;
    }

    public void setMatType(String matType) {
        this.matType = matType;
    }
}
