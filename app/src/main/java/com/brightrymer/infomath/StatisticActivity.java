package com.brightrymer.infomath;

/**
 * Created by Utilisateur on 04/11/2015.
 * For InfoMath18
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedHashMap;
import java.util.Random;

public class StatisticActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RelativeLayout rl_main_layout;
    Button btn_validate, btn_calculate, btn_random;
    EditText et_saisie_user, tab_stat_val_eff[];
    TextView tv_moyenne, tv_ecartype, tv_mode, tv_mediane;
    int id = 0;
    Miscellaneous misc = new Miscellaneous();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        rl_main_layout = (RelativeLayout) findViewById(R.id.serie_init);
        tv_moyenne = (TextView) findViewById(R.id.moyenne);
        tv_ecartype = (TextView) findViewById(R.id.ecart_type);
        tv_mode = (TextView) findViewById(R.id.mode_stat);
        tv_mediane = (TextView) findViewById(R.id.mediane_stat);

        btn_validate = (Button) findViewById(R.id.validate_stat);
        btn_calculate = (Button) findViewById(R.id.stat_calcul);
        btn_random = (Button) findViewById(R.id.random_stat);

        et_saisie_user = (EditText) findViewById(R.id.nb_valeur_stat_user);

        btn_validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    int val_saisie_user, modulo_val_user;

                    // Récupération valeur saisie par l'utilisateur : Nombre d'élements désirés.
                    val_saisie_user = Integer.parseInt(et_saisie_user.getText().toString());

                    if (val_saisie_user < 1 || val_saisie_user > 20) {

                        // On envoie une erreur pour non respect des normes.
                        throw new IllegalArgumentException(getString(R.string.err_non_compliant_element_number));
                    }

                    // On s'assure qu'on affiche pas les résultats
                    tv_moyenne.setVisibility(View.GONE);
                    tv_ecartype.setVisibility(View.GONE);
                    tv_mode.setVisibility(View.GONE);
                    tv_mediane.setVisibility(View.GONE);

                    // Déclaration de notre tableau(valeurs, effectifs, valeurs, effectifs, ... ) : Notre de paire 20 donc 40 cases
                    tab_stat_val_eff = new EditText[40];

                    // On s'assure que tous les entêtes de lignes sont masqués
                    for (int i = 0; i < 8; i++) {
                        findViewById(R.id.val_stat + i).setVisibility(View.GONE);
                    }
                    // On va chercher tous les EditText servant à saisir la série statistique.
                    for (int i = 0; i < 40; i++) {

                        tab_stat_val_eff[i] = (EditText) findViewById(R.id.edit_stat_1 + i);
                        tab_stat_val_eff[i].setVisibility(View.GONE);
                        tab_stat_val_eff[i].setText("");
                    }

                    // On affiche le nombre d'EditText correspondant au nombre demandé par l'utilisateur (entre 1 et 20)
                    for (int i = 0; i < (val_saisie_user * 2); i++) {

                        tab_stat_val_eff[i].setVisibility(View.VISIBLE);

                        // On donne le focus au premier EditText
                        if (i == 0) {
                            tab_stat_val_eff[i].requestFocus();
                        }

                        if (i == ((val_saisie_user * 2) - 1))
                            tab_stat_val_eff[i].setImeOptions(EditorInfo.IME_ACTION_DONE);
                    }

                    // On affiche le bouton pour lancer le calcul et le bouton de génération aléatoire
                    btn_calculate.setVisibility(View.VISIBLE);
                    btn_random.setVisibility(View.VISIBLE);

                    // On affiche les entêtes de lignes
                    int j = 0;
                    int tmp = (int) Math.ceil(((double) val_saisie_user) / 5);
                    switch (tmp) {
                        case 4:
                            j = 8;
                            break;
                        case 3:
                            j = 6;
                            break;
                        case 2:
                            j = 4;
                            break;
                        case 1:
                            j = 2;
                            break;
                    }

                    for (int i = 0; i < j; i++) {
                        findViewById(R.id.val_stat + i).setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {

                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_random.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    int nb_edittext_user;

                    // On cache le clavier
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rl_main_layout.getWindowToken(), 0);

                    // On note combien de valeur affichée à l'utilisateur (valeurs saisies par utilisateur * 2)
                    nb_edittext_user = Integer.parseInt(et_saisie_user.getText().toString());

                    LinkedHashMap<Double, Integer> valeurs_rand = new LinkedHashMap<>();

                    // Génération des valeurs aléatoires
                    Random randVal = new Random();

                    DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
                    symbols.setDecimalSeparator('.');
                    DecimalFormat myDoubleFormat = new DecimalFormat("#.##", symbols);

                    Double borneMinVal = -99.9, borneMaxVal = 99.9;
                    int borneMinEff = 1, borneMaxEff = 99;
                    Double resultValeur = borneMinVal + (Double) (Math.random() * ((borneMaxVal - borneMinVal) + 1));
                    Integer resultEff = borneMinEff + (int) (Math.random() * ((borneMaxEff - borneMinEff) + 1));

                    // On rempli la table de Hash de vérification et les EditText avec les valeurs générées aléatoirement
                    for (int i = 0; i < nb_edittext_user * 2; i = i + 2) {

                        if (valeurs_rand.containsKey(resultValeur)) { // On n'accepte pas de doublon de valeur

                            do {
                                resultValeur = borneMinVal + (borneMaxVal - borneMinVal) * randVal.nextDouble();
                            } while (!valeurs_rand.containsKey(resultValeur));
                        }

                        tab_stat_val_eff[i].setText(myDoubleFormat.format(resultValeur));
                        tab_stat_val_eff[i + 1].setText(resultEff);

                        // On alimente notre table de hash de vérification
                        valeurs_rand.put(resultValeur, resultEff);

                        // On génère une nouvelle paire valeurs/effectifs
                        resultValeur = borneMinVal + (Double) (Math.random() * ((borneMaxVal - borneMinVal) + 1));
                        resultEff = borneMinEff + (int) (Math.random() * ((borneMaxEff - borneMinEff) + 1));
                    }

                } catch (Exception e) {

                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    int nb_edittext_user;

                    hideKeyboard();

                    // On note combien de valeur affichée à l'utilisateur (valeurs saisies par utilisateur * 2)
                    nb_edittext_user = Integer.parseInt(et_saisie_user.getText().toString());

                    LinkedHashMap<Double, Integer> valeurs_user = new LinkedHashMap<>();

                    // On rempli la table de Hash avec les valeurs saisies par l'utilisateur
                    for (int i = 0; i < nb_edittext_user * 2; i = i + 2) {

                        Double val = Double.parseDouble(tab_stat_val_eff[i].getText().toString());
                        int eff = Integer.parseInt(tab_stat_val_eff[i + 1].getText().toString());
                        if (!valeurs_user.containsKey(val)) {
                            valeurs_user.put(val, eff);
                        } else {
                            tab_stat_val_eff[i].requestFocus();
                            throw new Exception(getString(R.string.err_duplicate));
                        }
                    }

                    Resultat resCalcul = misc.statistComplete(valeurs_user);

                    DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
                    symbols.setDecimalSeparator('.');
                    DecimalFormat myDoubleFormat = new DecimalFormat("#.###", symbols);

                    String res = getResources().getString(R.string.moyenne_stat) + " : " + myDoubleFormat.format(resCalcul.getMoyenne());
                    // On remplit les champs résultats et on les affiches
                    tv_moyenne.setText(res);
                    tv_moyenne.setVisibility(View.VISIBLE);

                    res = getResources().getString(R.string.stand_deviat) + " : " + myDoubleFormat.format(resCalcul.getEcartType());
                    tv_ecartype.setText(res);
                    tv_ecartype.setVisibility(View.VISIBLE);

                    res = getResources().getString(R.string.mode_stat) + " : " + resCalcul.getMode();
                    tv_mode.setText(res);
                    tv_mode.setVisibility(View.VISIBLE);

                    res = getResources().getString(R.string.median_stat) + " : " + myDoubleFormat.format(resCalcul.getMediane());
                    tv_mediane.setText(res);
                    tv_mediane.setVisibility(View.VISIBLE);

                } catch (Exception e) {

                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void hideKeyboard() {

        // On cache le clavier
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rl_main_layout.getWindowToken(), 0);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        id = item.getItemId();

        if (id == R.id.nav_sec_degree) {
            // Handle the second degree action
            try {
                Intent intent = new Intent(this, SecDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_third_degree) {
            // Handle the third degree action
            try {
                Intent intent = new Intent(this, ThirDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_stat) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, StatisticActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_unif_distrib) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, UniformActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_binom_distrib) {
            // Handle the binomial distribution action
            try {
                Intent intent = new Intent(this, BinomialActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_poiss_distrib) {
            // Handle the poisson distribution action
            try {
                Intent intent = new Intent(this, PoissonActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_norma_distrib) {
            // Handle the normal distribution action
            try {
                Intent intent = new Intent(this, NormalActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_test_khi) {

        } else if (id == R.id.nav_matrix) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, MatrixActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.license) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, LicenseActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
