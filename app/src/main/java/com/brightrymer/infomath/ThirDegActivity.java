package com.brightrymer.infomath;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class ThirDegActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RelativeLayout rl_main_layout;
    Button btn_thir_deg;
    EditText et_a, et_b, et_c, et_d;
    int id = 0;
    Miscellaneous misc = new Miscellaneous();
    String resString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thir_deg);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        rl_main_layout = (RelativeLayout) findViewById(R.id.equation_line);

        btn_thir_deg = (Button) findViewById(R.id.thir_deg_calc);

        et_a = (EditText) findViewById(R.id.edittext_a);
        et_b = (EditText) findViewById(R.id.edittext_b);
        et_c = (EditText) findViewById(R.id.edittext_c);
        et_d = (EditText) findViewById(R.id.edittext_d);

        btn_thir_deg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    double val_a, val_b, val_c, val_d;

                    val_a = Double.parseDouble(et_a.getText().toString());
                    val_b = Double.parseDouble(et_b.getText().toString());
                    val_c = Double.parseDouble(et_c.getText().toString());
                    val_d = Double.parseDouble(et_d.getText().toString());

                    DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
                    symbols.setDecimalSeparator('.');
                    DecimalFormat myDoubleFormat = new DecimalFormat("#.##", symbols);

                    Resultat resCalcul = misc.thirdDegree(val_a, val_b, val_c, val_d);

                    TextView discriminant = (TextView) findViewById(R.id.discriminant);
                    TextView discr = (TextView) findViewById(R.id.discriminant_result);
                    TextView discr_sens = (TextView) findViewById(R.id.discriminant_sens);
                    TextView equat_sol_x1 = (TextView) findViewById(R.id.equation_solution_x1);
                    TextView equat_sol_x2 = (TextView) findViewById(R.id.equation_solution_x2);
                    TextView equat_sol_x3 = (TextView) findViewById(R.id.equation_solution_x3);

                    discriminant.setText(getResources().getString(R.string.discriminant));

                    resString = " " + myDoubleFormat.format(resCalcul.getDiscriminant());
                    discr.setText(resString);
                    equat_sol_x1.setText("");
                    equat_sol_x2.setText("");
                    equat_sol_x3.setText("");
                    discr_sens.setText("");

                    hideKeyboard();
                    findViewById(R.id.discriminant).setVisibility(View.VISIBLE);

                    if (resCalcul.getType().equals("Thir Deg 3.2")) { // Cas b = 0 et c = 0 donc pas de discriminant à afficher.

                        discriminant.setText(getString(R.string.msg_b_c_equal_0)); // On recycle les TextView.
                        discr.setText("");
                        discr_sens.setText(getString(R.string.msg_3_sol));

                        resString = "x1 = " + myDoubleFormat.format(resCalcul.getX1());
                        equat_sol_x1.setText(resString);

                        resString = "x2 = " + myDoubleFormat.format(resCalcul.getX2()) + " + i" + myDoubleFormat.format(Math.abs(resCalcul.getX3()));
                        equat_sol_x2.setText(resString);

                        resString = "x3 = " + myDoubleFormat.format(resCalcul.getX2()) + " - i" + myDoubleFormat.format(Math.abs(resCalcul.getX3()));
                        equat_sol_x3.setText(resString);
                    }
                    if (resCalcul.getType().equals("Thir Deg 3.1")) {

                        resString = getResources().getString(R.string.discriminant_sign) + " > 0 : " + getString(R.string.msg_3_sol);
                        discr_sens.setText(resString);

                        resString = "x1 = " + myDoubleFormat.format(resCalcul.getX1());
                        equat_sol_x1.setText(resString);

                        resString = "x2 = " + myDoubleFormat.format(resCalcul.getX2()) + " + i" + myDoubleFormat.format(Math.abs(resCalcul.getX3()));
                        equat_sol_x2.setText(resString);

                        resString = "x3 = " + myDoubleFormat.format(resCalcul.getX2()) + " - i" + myDoubleFormat.format(Math.abs(resCalcul.getX3()));
                        equat_sol_x3.setText(resString);
                    }
                    if (resCalcul.getType().equals("Thir Deg 3") || resCalcul.getType().equals("Thir Deg 2")) {

                        if (resCalcul.getType().equals("Thir Deg 3")) {

                            resString = getResources().getString(R.string.discriminant_sign) + " < 0 : " + getString(R.string.msg_3_sol);
                            discr_sens.setText(resString);
                        } else {

                            resString = getResources().getString(R.string.discriminant_sign) + " = 0 : Deux solutions distinctes (x1, x2 = x3).";
                            discr_sens.setText(resString);
                        }

                        resString = "x1 = " + myDoubleFormat.format(resCalcul.getX1());
                        equat_sol_x1.setText(resString);

                        resString = "x2 = " + myDoubleFormat.format(resCalcul.getX2());
                        equat_sol_x2.setText(resString);

                        resString = "x3 = " + myDoubleFormat.format(resCalcul.getX3());
                        equat_sol_x3.setText(resString);
                    }
                    if (resCalcul.getType().equals("Sec Deg 2")) {

                        resString = getResources().getString(R.string.discriminant_sign) + " > 0 : " + getString(R.string.msg_2_sol_eq_sec_deg);
                        discr_sens.setText(resString);

                        resString = "x1 = " + myDoubleFormat.format(resCalcul.getX1()) + ".";
                        equat_sol_x1.setText(resString);

                        resString = "x2 = " + myDoubleFormat.format(resCalcul.getX2()) + ".";
                        equat_sol_x2.setText(resString);
                    } else {
                        if (resCalcul.getType().equals("Sec Deg 1")) {

                            resString = getResources().getString(R.string.discriminant_sign) + " = 0 : " + getString(R.string.msg_1_sol_eq_sec_deg);
                            discr_sens.setText(resString);

                            resString = "x1 = " + myDoubleFormat.format(resCalcul.getX1()) + ".";
                            equat_sol_x1.setText(resString);
                        } else {
                            if (resCalcul.getType().equals("Sec Deg 0")) {

                                resString = getResources().getString(R.string.discriminant_sign) + " < 0 : " + getString(R.string.msg_2_sol_eq_sec_comp);
                                discr_sens.setText(resString);

                                resString = "x1 = " + myDoubleFormat.format(resCalcul.getX1()) + " + i" + myDoubleFormat.format(Math.abs(resCalcul.getX2()));
                                equat_sol_x1.setText(resString);

                                resString = "x2 = " + myDoubleFormat.format(resCalcul.getX1()) + " - i" + myDoubleFormat.format(Math.abs(resCalcul.getX2()));
                                equat_sol_x2.setText(resString);
                            }
                        }
                    }
                } catch (Exception e) {

                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void hideKeyboard() {

        // On cache le clavier
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rl_main_layout.getWindowToken(), 0);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        id = item.getItemId();

        if (id == R.id.nav_sec_degree) {
            // Handle the second degree action
            try {
                Intent intent = new Intent(this, SecDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_third_degree) {
            // Handle the third degree action
            try {
                Intent intent = new Intent(this, ThirDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_stat) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, StatisticActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_unif_distrib) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, UniformActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_binom_distrib) {
            // Handle the binomial distribution action
            try {
                Intent intent = new Intent(this, BinomialActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_poiss_distrib) {
            // Handle the poisson distribution action
            try {
                Intent intent = new Intent(this, PoissonActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_norma_distrib) {
            // Handle the normal distribution action
            try {
                Intent intent = new Intent(this, NormalActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_test_khi) {

        } else if (id == R.id.nav_matrix) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, MatrixActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.license) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, LicenseActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
