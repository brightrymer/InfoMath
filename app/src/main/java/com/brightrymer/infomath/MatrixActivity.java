package com.brightrymer.infomath;

/**
 * Created by Utilisateur on 04/11/2015.
 * For InfoMath18
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

public class MatrixActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RelativeLayout rl_main_layout, rl_matrix_a_init, rl_matrix_result, rl_matrix_a_result, rl_matrix_b_result;
    Button btn_calculate, btn_a_validate;
    TextView tv_signe_result, tv_signe_equal;
    RadioGroup rg_group_operation, rg_matrix_size;
    String typeOperation = "";
    List<Double> matA = new ArrayList<>(), matB = new ArrayList<>();
    boolean btnAValidateClicked = false;
    int id = 0;
    Miscellaneous misc = new Miscellaneous();
    Resultat matRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matrix);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        rl_main_layout = (RelativeLayout) findViewById(R.id.serie_init);
        rl_matrix_a_init = (RelativeLayout) findViewById(R.id.matrix_a_init);
        rl_matrix_result = (RelativeLayout) findViewById(R.id.matrix_result);
        rl_matrix_a_result = (RelativeLayout) findViewById(R.id.matrix_a_result);
        rl_matrix_b_result = (RelativeLayout) findViewById(R.id.matrix_b_result);

        rg_group_operation = (RadioGroup) findViewById(R.id.matrix_group_operation);
        rg_matrix_size = (RadioGroup) findViewById(R.id.matrix_size_choice);

        btn_a_validate = (Button) findViewById(R.id.matrix_a_validate);
        btn_calculate = (Button) findViewById(R.id.matrix_calcul);
        tv_signe_result = (TextView) findViewById(R.id.matrix_signe_result);
        tv_signe_equal = (TextView) findViewById(R.id.matrix_equal_result);

        rg_matrix_size.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.matrix_size_2) {

                    try {

                        typeOperation = "Mat2";
                        matA.clear();
                        matB.clear();
                        btnAValidateClicked = false;
                        hideKeyboard();

                        rg_group_operation.clearCheck();
                        rg_group_operation.getChildAt(2).setVisibility(View.VISIBLE);
                        hideResetElements();

                        Toast.makeText(getApplicationContext(), getString(R.string.err_choose_operation), Toast.LENGTH_SHORT).show();
                        rg_group_operation.setVisibility(View.VISIBLE);
                    } catch (Exception e) {

                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else if (checkedId == R.id.matrix_size_3) {

                    try {

                        typeOperation = "Mat3";
                        matA.clear();
                        matB.clear();
                        btnAValidateClicked = false;
                        hideKeyboard();

                        rg_group_operation.clearCheck();
                        rg_group_operation.getChildAt(2).setVisibility(View.GONE);
                        hideResetElements();

                        Toast.makeText(getApplicationContext(), getString(R.string.err_choose_operation), Toast.LENGTH_SHORT).show();
                        rg_group_operation.setVisibility(View.VISIBLE);

                    } catch (Exception e) {

                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        rg_group_operation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId != -1) {

                    if (checkedId == R.id.addition_matrix || checkedId == R.id.substract_matrix) {

                        if (!btnAValidateClicked) {

                            matA.clear();
                            matB.clear();
                            btn_calculate.setVisibility(View.GONE);
                            btn_a_validate.setVisibility(View.VISIBLE);
                        }

                        if (typeOperation.equals("Mat2")) {

                            showElement(2);
                        } else if (typeOperation.equals("Mat3")) {

                            showElement(3);
                        }
                    } else { // Inversion

                        if (btnAValidateClicked) {

                            hideResetElements();
                            btnAValidateClicked = false;
                            matA.clear();
                        }

                        if (typeOperation.equals("Mat2")) {

                            showElement(2);
                        } else if (typeOperation.equals("Mat3")) {

                            showElement(3);
                        }

                        btn_a_validate.setVisibility(View.GONE);
                        btn_calculate.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        btn_a_validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (rg_group_operation.getCheckedRadioButtonId() == -1) {

                        throw new IllegalArgumentException(getString(R.string.err_choose_operation));
                    } else {

                        matA.clear();
                        if (typeOperation.equals("Mat2")) {

                            for (int i = 0; i < 6; i++) {

                                if (i == 0) {

                                    TextView tv_tmp = (TextView) rl_matrix_a_init.getChildAt(i);
                                    tv_tmp.setText(getString(R.string.matrix_a));
                                } else {
                                    if (i != 3) {

                                        EditText et_tmp = (EditText) rl_matrix_a_init.getChildAt(i);
                                        Double val;

                                        try {

                                            // On récupère données saisie utilisateur
                                            val = Double.parseDouble(et_tmp.getText().toString());
                                        } catch (Exception e) {

                                            et_tmp.requestFocus();
                                            throw new IllegalArgumentException(getString(R.string.err_value_empty));
                                        }

                                        matA.add(val);
                                    }
                                }
                            }

                            btnAValidateClicked = true;
                            btn_a_validate.setVisibility(View.GONE);

                            if (rg_group_operation.getCheckedRadioButtonId() != R.id.invert_matrix) { // On affiche la deuxième matrice de saisie s'il ne s'agit pas d'une inversion

                                TextView tv_tmp = (TextView) rl_matrix_a_init.getChildAt(0);
                                hideResetElements();

                                tv_tmp.setText(getString(R.string.matrix_b));
                                showElement(2);

                                btn_calculate.setVisibility(View.VISIBLE);
                            }

                        } else {
                            if (typeOperation.equals("Mat3")) {

                                for (int i = 0; i < 10; i++) {

                                    if (i == 0) {

                                        TextView tv_tmp = (TextView) rl_matrix_a_init.getChildAt(i);
                                        tv_tmp.setText(getString(R.string.matrix_a));
                                    } else {

                                        EditText et_tmp = (EditText) rl_matrix_a_init.getChildAt(i);
                                        Double val;

                                        try {

                                            // On récupère données saisie utilisateur
                                            val = Double.parseDouble(et_tmp.getText().toString());
                                        } catch (Exception e) {

                                            et_tmp.requestFocus();
                                            throw new IllegalArgumentException(getString(R.string.err_value_empty));
                                        }

                                        matA.add(val);
                                    }
                                }

                                btnAValidateClicked = true;
                                btn_a_validate.setVisibility(View.GONE);

                                if (rg_group_operation.getCheckedRadioButtonId() != R.id.invert_matrix) { // On affiche la deuxième matrice de saisie s'il ne s'agit pas d'une inversion

                                    TextView tv_tmp = (TextView) rl_matrix_a_init.getChildAt(0);
                                    hideResetElements();

                                    tv_tmp.setText(getString(R.string.matrix_b));
                                    showElement(3);

                                    btn_calculate.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }
                } catch (Exception e) {

                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    matB.clear();

                    if (typeOperation.equals("Mat2")) {

                        for (int i = 0; i < 6; i++) {

                            if (i != 0) {

                                if (i != 3) {

                                    EditText et_tmp = (EditText) rl_matrix_a_init.getChildAt(i);
                                    Double val;

                                    try {

                                        // On récupère données saisie utilisateur
                                        val = Double.parseDouble(et_tmp.getText().toString());
                                    } catch (Exception e) {

                                        et_tmp.requestFocus();
                                        throw new IllegalArgumentException(getString(R.string.err_value_empty));
                                    }

                                    matB.add(val);
                                }
                            }
                        }

                        if (rg_group_operation.getCheckedRadioButtonId() == R.id.addition_matrix) {

                            matRes = misc.calculMatriceAdd(matA, matB);
                        }

                        if (rg_group_operation.getCheckedRadioButtonId() == R.id.substract_matrix) {

                            matRes = misc.calculMatriceSub(matA, matB);
                        }

                        if (rg_group_operation.getCheckedRadioButtonId() == R.id.invert_matrix) {

                            matRes = misc.calculMatriceInvert(matB);
                        }

                        showMatRes(matRes);
                    } else {

                        if (typeOperation.equals("Mat3")) {

                            for (int i = 0; i < 10; i++) {

                                if (i != 0) {

                                    EditText et_tmp = (EditText) rl_matrix_a_init.getChildAt(i);
                                    Double val;

                                    try {

                                        // On récupère données saisie utilisateur
                                        val = Double.parseDouble(et_tmp.getText().toString());
                                    } catch (Exception e) {

                                        et_tmp.requestFocus();
                                        throw new IllegalArgumentException(getString(R.string.err_value_empty));
                                    }

                                    matB.add(val);
                                }
                            }

                            if (rg_group_operation.getCheckedRadioButtonId() == R.id.addition_matrix) {

                                matRes = misc.calculMatriceAdd(matA, matB);
                            } else if (rg_group_operation.getCheckedRadioButtonId() == R.id.substract_matrix) {

                                matRes = misc.calculMatriceSub(matA, matB);
                            }

                            showMatRes(matRes);
                        }
                    }

                    hideKeyboard();
                } catch (Exception e) {

                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showMatRes(Resultat matRes) {

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setDecimalSeparator('.');
        DecimalFormat myDf = new DecimalFormat("#.###", symbols);

        hideKeyboard();

        if (rg_group_operation.getCheckedRadioButtonId() == R.id.invert_matrix) {

            tv_signe_result.setVisibility(View.GONE);
        } else {

            tv_signe_result.setVisibility(View.GONE);
        }

        if (rg_group_operation.getCheckedRadioButtonId() == R.id.addition_matrix) {

            tv_signe_result.setText(getString(R.string.signe_plus));
            tv_signe_result.setVisibility(View.VISIBLE);
        }

        if (rg_group_operation.getCheckedRadioButtonId() == R.id.substract_matrix) {

            tv_signe_result.setText(getString(R.string.signe_moins));
            tv_signe_result.setVisibility(View.VISIBLE);
        }

        tv_signe_equal.setVisibility(View.VISIBLE);

        if (matRes.getMatType().equals("Mat2")) {

            for (int i = 0; i < 4; i++) {

                if (i == 2) {

                    TextView et_tmp = (TextView) rl_matrix_result.getChildAt(i + 1);
                    et_tmp.setText(myDf.format(matRes.getMatResultat().get(i)));
                    et_tmp.setVisibility(View.VISIBLE);

                    if (rg_group_operation.getCheckedRadioButtonId() != R.id.invert_matrix) {

                        et_tmp = (TextView) rl_matrix_a_result.getChildAt(i + 1);
                        et_tmp.setText(myDf.format(matA.get(i)));
                        et_tmp.setVisibility(View.VISIBLE);
                    }

                    et_tmp = (TextView) rl_matrix_b_result.getChildAt(i + 1);
                    et_tmp.setText(myDf.format(matB.get(i)));
                    et_tmp.setVisibility(View.VISIBLE);
                }
                if (i != 2) {

                    if (i == 3) {

                        TextView et_tmp = (TextView) rl_matrix_result.getChildAt(i + 1);
                        et_tmp.setText(myDf.format(matRes.getMatResultat().get(i)));
                        et_tmp.setVisibility(View.VISIBLE);

                        if (rg_group_operation.getCheckedRadioButtonId() != R.id.invert_matrix) {

                            et_tmp = (TextView) rl_matrix_a_result.getChildAt(i + 1);
                            et_tmp.setText(myDf.format(matA.get(i)));
                            et_tmp.setVisibility(View.VISIBLE);
                        }

                        et_tmp = (TextView) rl_matrix_b_result.getChildAt(i + 1);
                        et_tmp.setText(myDf.format(matB.get(i)));
                        et_tmp.setVisibility(View.VISIBLE);
                    } else {

                        TextView et_tmp = (TextView) rl_matrix_result.getChildAt(i);
                        et_tmp.setText(myDf.format(matRes.getMatResultat().get(i)));
                        et_tmp.setVisibility(View.VISIBLE);

                        if (rg_group_operation.getCheckedRadioButtonId() != R.id.invert_matrix) {

                            et_tmp = (TextView) rl_matrix_a_result.getChildAt(i);
                            et_tmp.setText(myDf.format(matA.get(i)));
                            et_tmp.setVisibility(View.VISIBLE);
                        }

                        et_tmp = (TextView) rl_matrix_b_result.getChildAt(i);
                        et_tmp.setText(myDf.format(matB.get(i)));
                        et_tmp.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

        if (matRes.getMatType().equals("Mat3")) {

            for (int i = 0; i < 9; i++) {

                TextView et_tmp = (TextView) rl_matrix_result.getChildAt(i);
                et_tmp.setText(myDf.format(matRes.getMatResultat().get(i)));
                et_tmp.setVisibility(View.VISIBLE);

                if (rg_group_operation.getCheckedRadioButtonId() != R.id.invert_matrix) {

                    et_tmp = (TextView) rl_matrix_a_result.getChildAt(i);
                    et_tmp.setText(myDf.format(matA.get(i)));
                    et_tmp.setVisibility(View.VISIBLE);
                }

                et_tmp = (TextView) rl_matrix_b_result.getChildAt(i);
                et_tmp.setText(myDf.format(matB.get(i)));
                et_tmp.setVisibility(View.VISIBLE);
            }
        }
    }

    private void hideKeyboard() {

        // On cache le clavier
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rl_main_layout.getWindowToken(), 0);
    }

    private void showElement(int typeMatrice) {

        if (typeMatrice == 2) {
            for (int i = 0; i < 6; i++) {

                if (i != 3) {

                    if (i != 0) {

                        EditText et_tmp = (EditText) rl_matrix_a_init.getChildAt(i);
                        et_tmp.setVisibility(View.VISIBLE);

                        if (i == 5) {

                            et_tmp.setImeOptions(EditorInfo.IME_ACTION_DONE);
                        }
                    } else {

                        rl_matrix_a_init.getChildAt(i).setVisibility(View.VISIBLE);
                    }
                }
            }
        }

        if (typeMatrice == 3) {

            for (int i = 0; i < 10; i++) {

                if (i != 0) {

                    EditText et_tmp = (EditText) rl_matrix_a_init.getChildAt(i);
                    et_tmp.setVisibility(View.VISIBLE);

                    if (i == 5) {

                        et_tmp.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                    }
                } else {

                    rl_matrix_a_init.getChildAt(i).setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void hideResetElements() {

        for (int i = 0; i < 10; i++) {

            EditText et_tmp;
            TextView tv_tmp;
            if (i != 0) {

                et_tmp = (EditText) rl_matrix_a_init.getChildAt(i);
                et_tmp.setText("");
                et_tmp.setVisibility(View.GONE);

                if (i < 9) { // Reset des matrices résultats

                    tv_tmp = (TextView) rl_matrix_a_result.getChildAt(i);
                    tv_tmp.setText("");
                    tv_tmp.setVisibility(View.GONE);

                    tv_tmp = (TextView) rl_matrix_b_result.getChildAt(i);
                    tv_tmp.setText("");
                    tv_tmp.setVisibility(View.GONE);

                    tv_tmp = (TextView) rl_matrix_result.getChildAt(i);
                    tv_tmp.setText("");
                    tv_tmp.setVisibility(View.GONE);
                }
            } else {

                tv_tmp = (TextView) rl_matrix_a_init.getChildAt(i);
                tv_tmp.setVisibility(View.GONE);
                tv_tmp.setText(getString(R.string.matrix_a));

                // Reset des matrices résultats
                tv_tmp = (TextView) rl_matrix_a_result.getChildAt(i);
                tv_tmp.setText("");
                tv_tmp.setVisibility(View.GONE);

                tv_tmp = (TextView) rl_matrix_b_result.getChildAt(i);
                tv_tmp.setText("");
                tv_tmp.setVisibility(View.GONE);

                tv_tmp = (TextView) rl_matrix_result.getChildAt(i);
                tv_tmp.setText("");
                tv_tmp.setVisibility(View.GONE);
            }
        }
        btn_a_validate.setVisibility(View.GONE);
        btn_calculate.setVisibility(View.GONE);
        tv_signe_result.setVisibility(View.GONE);
        tv_signe_equal.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        id = item.getItemId();

        if (id == R.id.nav_sec_degree) {
            // Handle the second degree action
            try {
                Intent intent = new Intent(this, SecDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_third_degree) {
            // Handle the third degree action
            try {
                Intent intent = new Intent(this, ThirDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_stat) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, StatisticActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_unif_distrib) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, UniformActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_binom_distrib) {
            // Handle the binomial distribution action
            try {
                Intent intent = new Intent(this, BinomialActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_poiss_distrib) {
            // Handle the poisson distribution action
            try {
                Intent intent = new Intent(this, PoissonActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_norma_distrib) {
            // Handle the normal distribution action
            try {
                Intent intent = new Intent(this, NormalActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_test_khi) {

        } else if (id == R.id.nav_matrix) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, MatrixActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.license) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, LicenseActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}