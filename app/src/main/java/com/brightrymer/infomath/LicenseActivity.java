package com.brightrymer.infomath;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class LicenseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView tv_license_txt;
    int id = 0;
    String fileContent = "", line = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        tv_license_txt = (TextView) findViewById(R.id.license_txt);

        try {

            AssetManager myAM = getAssets();
            InputStream myInputS = myAM.open("LICENSE.txt");

            InputStreamReader myStreamReader = new InputStreamReader(myInputS);
            BufferedReader myBuffR = new BufferedReader(myStreamReader);

            while ((line = myBuffR.readLine()) != null) {

                if (fileContent.equals("")) {

                    fileContent = line;
                } else {

                    fileContent += "\n" + line;
                }
            }

            tv_license_txt.setText(fileContent);
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        id = item.getItemId();

        if (id == R.id.nav_sec_degree) {
            // Handle the second degree action
            try {
                Intent intent = new Intent(this, LicenseActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_third_degree) {
            // Handle the third degree action
            try {
                Intent intent = new Intent(this, ThirDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_stat) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, StatisticActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_unif_distrib) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, UniformActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_binom_distrib) {
            // Handle the binomial distribution action
            try {
                Intent intent = new Intent(this, BinomialActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_poiss_distrib) {
            // Handle the poisson distribution action
            try {
                Intent intent = new Intent(this, PoissonActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_norma_distrib) {
            // Handle the normal distribution action
            try {
                Intent intent = new Intent(this, NormalActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_test_khi) {

        } else if (id == R.id.nav_matrix) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, MatrixActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
