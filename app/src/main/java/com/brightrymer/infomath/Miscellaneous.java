package com.brightrymer.infomath;

import android.app.Activity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by brigh on 27/05/2016.
 * For InfoMath18
 */
public class Miscellaneous extends Activity {

    private static Map<Double, Double> sortByComparator(Map<Double, Double> unsortMap) {

        // Convert Map to List
        List<Map.Entry<Double, Double>> list = new LinkedList<>(unsortMap.entrySet());

        // Sort list with comparator, to compare the Map values
        Collections.sort(list, new Comparator<Map.Entry<Double, Double>>() {
            public int compare(Map.Entry<Double, Double> o1,
                               Map.Entry<Double, Double> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // Convert sorted map back to a Map
        Map<Double, Double> sortedMap = new LinkedHashMap<>();
        for (Iterator<Map.Entry<Double, Double>> it = list.iterator(); it.hasNext(); ) {
            Map.Entry<Double, Double> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    public static void printMap(Map<Double, Double> map) {
        for (Map.Entry<Double, Double> entry : map.entrySet()) {
            System.out.println("[Key] : " + entry.getKey()
                    + " [Value] : " + entry.getValue());
        }
    }

    // Polynome du second dégré
    public Resultat secondDegree(Double a, Double b, Double c) {

        Double discrimant, x1, x2;
        Resultat resSecDeg;
        String typeResult;

        if ((a == null) || (b == null) || (c == null)) {

            throw new IllegalArgumentException();
        }

        // Calcul Discriminant
        discrimant = Math.pow(b, 2.0) - (4.0 * a * c);

        if (discrimant > 0) {

            typeResult = "Sec Deg 2";

            x1 = (-b - Math.sqrt(discrimant)) / (2.0 * a);
            x2 = (-b + Math.sqrt(discrimant)) / (2.0 * a);
        } else {
            if (discrimant == 0) {

                typeResult = "Sec Deg 1";

                x1 = (-b) / (2.0 * a);
                x2 = null; //Pas nécessaire : Solution double x2 = x1
            } else { // Discriminant négatif

                typeResult = "Sec Deg 0";

                x1 = (-b) / (2 * a); // Partie réelle
                x2 = Math.sqrt(-discrimant) / (2 * a); // Partie imaginaire
            }
        }

        resSecDeg = new Resultat(typeResult, discrimant, x1, x2);

        return resSecDeg;
    }

    // Polynome du troisième degré
    public Resultat thirdDegree(Double a, Double b, Double c, Double d) {

        Double discrimant, x, x1, x2, x3;
        Resultat resSecDeg;
        Resultat resThirDeg;
        String typeResult;
        discrimant = 0.0;


        if ((a == null) || (b == null) || (c == null) || (d == null)) {

            throw new IllegalArgumentException();
        }

        // Equation du second dégré ?
        if ((a == 0)) { // Si oui

            // Dans ce cas, on appel résolution degré 2 avec décalage de variable : A = B, B = C, C = D
            return secondDegree(b, c, d);
        }

        if ((b == 0) && (c == 0)) {

            x = (-d / a);
            x1 = signe(x) * Math.pow(Math.abs(x), (1.0 / 3.0));
            x2 = -x1 / 2.0; // Partie réelle
            x3 = (Math.sqrt(3.0) / 2.0) * Math.abs(x1); // Partie imaginaire
            typeResult = "Thir Deg 3.2";
            return new Resultat(typeResult, discrimant, x1, x2, x3);
        }

        // Algo général
        Double a0, a1, a2, a3, p, q;
        a0 = d / a;
        a1 = c / a;
        a2 = b / a;
        a3 = a2 / 3.0;
        p = a1 - a3 * a2;
        q = a0 - a1 * a3 + 2.0 * Math.pow(a3, 3.0);

        // Calcul Discriminant
        discrimant = Math.pow(q / 2.0, 2.0) + Math.pow(p / 3.0, 3.0);

        if (discrimant > 0) {

            Double w1, w;
            typeResult = "Thir Deg 3.1";

            w1 = -q / 2.0 + Math.sqrt(discrimant);
            w = signe(w1) * Math.pow(Math.abs(w1), (1.0 / 3.0));

            x1 = w - (p / (3.0 * w)) - a3;
            x2 = -(a2 + x1) / 2.0; // Partie réelle
            x3 = Math.sqrt(3.0) / 2.0 * (w + (p / (3.0 * w))); // Partie imaginaire
        } else {
            if (discrimant == 0) {

                typeResult = "Thir Deg 2";

                x1 = (3.0 * q) / p - a3;
                x2 = (-3.0 * q) / (2.0 * p) - a3;
                x3 = x2;
            } else { // Discriminant négatif

                Double u, v, t;
                typeResult = "Thir Deg 3";

                // Calcul intermédiaire
                u = 2.0 * Math.sqrt(-p / 3.0);
                v = (-q) / (2.0 * (Math.pow((-p / 3.0), (3.0 / 2.0))));
                t = Math.acos(v) / 3.0;

                x1 = u * Math.cos(t) - a3;
                x2 = u * Math.cos(t + ((2.0 * Math.PI) / 3.0)) - a3;
                x3 = u * Math.cos(t + ((4.0 * Math.PI) / 3.0)) - a3;
            }
        }

        resThirDeg = new Resultat(typeResult, discrimant, x1, x2, x3);

        return resThirDeg;
    }

    // Moyenne, Ecart-type, Mode & Médiane
    public Resultat statistComplete(HashMap<Double, Integer> tabValeur) {

        Resultat resStat;
        Double valeurs = 0.0, moyenne, ecartType, mediane;
        int effectifs = 0;
        String mode;

        // Calcul moyenne pondérée
        for (Map.Entry<Double, Integer> element : tabValeur.entrySet()) {
            // valeur du singleton | effectif du singleton
            valeurs = valeurs + (element.getKey() * element.getValue());
            effectifs = effectifs + element.getValue();
        }
        moyenne = valeurs / effectifs;

        // Calcul écart-type
        ecartType = calculEcartType(effectifs, moyenne, tabValeur);

        // Calcul de la médiane
        mediane = calculMedianePond(effectifs, tabValeur);

        // Calcul du mode
        mode = calculMode(tabValeur);

        return new Resultat(moyenne, ecartType, mode, mediane);
    }

    private Double calculEcartType(int eff, Double moy, HashMap<Double, Integer> tab) {

        Double res = 0.0;
        for (Map.Entry<Double, Integer> element : tab.entrySet()) {

            res = res + element.getValue() * Math.pow((element.getKey() - moy), 2.0);
        }
        res /= eff;

        return Math.sqrt(res);
    }

    // Retourne le signe du chiffre en paramètre
    public Double signe(Double x) {
        Double res;

        if (x != null) {
            if (x < 0) {

                res = -1.0;
            } else {
                res = 1.0;
            }
        } else throw new IllegalArgumentException();

        return res;
    }

    public String calculMode(HashMap<Double, Integer> tab) {

        //Map<Double, Double> sortedByKeyMap = sortByComparator(tab);
        String res = "";

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setDecimalSeparator('.');
        DecimalFormat myDoubleFormat = new DecimalFormat("#.##", symbols);

        int maxValueInMap = Collections.max(tab.values());  // This will return max value in the Hashmap

        // On classe la map (tableau valeurs/effectifs) par ordre croissant des valeurs.
        Map<Double, Integer> sortedValUser = new TreeMap<>(tab);

        // On liste les valeurs et les effectifs
        List<Double> listValeur = new ArrayList<>(sortedValUser.keySet());
        List<Integer> listEffectif = new ArrayList<>(sortedValUser.values());

        for (int i = 0; i < listValeur.size(); i++) {  // Boucle sur la Map

            if (listEffectif.get(i).doubleValue() == maxValueInMap) {

                if (res.equals("")) {
                    res = myDoubleFormat.format(listValeur.get(i));
                } else {
                    res = res + ", " + myDoubleFormat.format(listValeur.get(i));     // On enregistre les clefs avec la valeur max
                }
            }
        }

        return res;
    }

    // Retourne la médiane de la map (valeurs/effectifs) passée en paramètre.
    public Double calculMedianePond(int eff, HashMap<Double, Integer> tab) {

        Double res;
        int effectifsInt = (int) Math.ceil(eff), count = 0, effectifsCumul = 0;

        // On classe la map (tableau valeurs/effectifs) par ordre croissant des valeurs.
        Map<Double, Integer> sortedValUser = new TreeMap<>(tab);

        // On liste les valeurs et les effectifs
        List<Double> listValeur = new ArrayList<>(sortedValUser.keySet());
        List<Integer> listEffectif = new ArrayList<>(sortedValUser.values());

        if (eff % 2 == 0) {

            Double n2, nSur2Plus1;
            int indexN2, indexNSur2Plus1;

            // On cherche dans quel valeur se trouve l'effectif N/2 par comptage
            for (int i = 0; i < listEffectif.size(); i++) {

                if (effectifsCumul >= (effectifsInt / 2)) {

                    break;
                } else {

                    effectifsCumul += listEffectif.get(i);
                    count++;
                }
            }
            indexN2 = count;

            count = 0;
            effectifsCumul = 0;
            // On cherche dans quel valeur se trouve l'effectif (N/2)+1 par comptage
            for (int i = 0; i < listEffectif.size(); i++) {

                if (effectifsCumul >= ((effectifsInt / 2) + 1)) {

                    break;
                } else {

                    effectifsCumul += listEffectif.get(i);
                    count++;
                }
            }
            indexNSur2Plus1 = count;

            // On récupère les valeurs N/2 et N/2 + 1
            n2 = listValeur.get(indexN2 - 1);
            nSur2Plus1 = listValeur.get(indexNSur2Plus1 - 1);

            return (n2 + nSur2Plus1) / 2;
        } else {

            // On cherche dans quel valeur se trouve l'effectif (N + 1)/2
            for (int i = 0; i < listEffectif.size(); i++) {

                if (effectifsCumul >= ((effectifsInt + 1) / 2)) {

                    break;
                } else {

                    effectifsCumul += listEffectif.get(i);
                    count++;
                }
            }

            // On récupère la valeur (N + 1)/2
            return listValeur.get(count - 1);
        }
    }

    public BigInteger factorielle(int N) {
        BigInteger multi = BigInteger.ONE;

        for (int i = 1; i <= N; i++) {

            multi = multi.multiply(BigInteger.valueOf(i));
        }
        return multi;
    }

    public BigDecimal calculPoisson(int lambda, int k) {

        BigDecimal res;
        Double e = 2.71828;

        BigDecimal comp1 = BigDecimal.valueOf(Math.pow(e, (-lambda))); // e^(-lambda)
        BigDecimal comp2 = BigDecimal.valueOf(Math.pow(lambda, k)); // lambda^(k)
        BigInteger fact = factorielle(k); // Factorielle(k)

        MathContext mc = new MathContext(2, RoundingMode.HALF_UP);

        res = (comp1.multiply(comp2)).divide(new BigDecimal(fact), mc);

        return res;
    }

    public BigDecimal calculBinomial(int n, Double p, int k) {

        BigDecimal res;
        MathContext mc = new MathContext(2, RoundingMode.HALF_UP);

        BigDecimal q = BigDecimal.valueOf(1 - p);
        BigInteger nMinK = factorielle(n - k);
        BigInteger divider = factorielle(k).multiply(nMinK);

        BigDecimal bigP = BigDecimal.valueOf(p);

        BigDecimal pK = bigP.pow(k, mc);
        BigDecimal qNMinusK = q.pow(n - k, mc);

        BigDecimal binCoeff = new BigDecimal(factorielle(n)).divide(new BigDecimal(divider), mc);

        res = (binCoeff.multiply(pK)).multiply(qNMinusK);

        return res;
    }

    public Double calculNormal(Double esperance, Double ecartType, int k) {

        Double res;
        MathContext mc = new MathContext(2, RoundingMode.HALF_UP);

        Double comp1 = 1 / (ecartType * Math.sqrt(2 * Math.PI));
        Double comp2 = Math.pow((k - esperance), 2);
        Double comp3 = Math.pow((ecartType), 2);

        comp2 = ((-1.0 / 2) * comp2) / comp3;

        res = comp1 * Math.exp(comp2);

        return res;
    }

    public Double calculUniform(int a, int b) {

        Double res;

        res = 1.0 / (b - a);

        return res;
    }

    public Double probaInRange(LinkedHashMap<Integer, Double> resultXProba, int borneMin, int borneMax) {

        Double resultat = 0.0;

        // On classe la map (tableau valeurs/effectifs) par ordre croissant des valeurs.
        Map<Integer, Double> sortedResult = new TreeMap<>(resultXProba);

        // On liste les k et leur probabilité respective
        List<Integer> listK = new ArrayList<>(sortedResult.keySet());
        List<Double> listKProba = new ArrayList<>(sortedResult.values());

        for (int i = borneMin; i <= borneMax; i++) {

            resultat = resultat + listKProba.get(i);
        }

        if (resultat > 1) { // Pas nécessaire de continuer : probabilité maximale.

            resultat = 1.0;
        }

        if (resultat < 0) {

            resultat = 0.0;
        }

        return resultat;
    }

    // Retourne une liste contenant les valeurs additionnée MatA et MatB
    public Resultat calculMatriceAdd(List<Double> matA, List<Double> matB) {

        Resultat matRes;
        List<Double> res = new ArrayList<>();

        for (int i = 0; i < matA.size(); i++) {

            res.add(matA.get(i) + matB.get(i));
        }

        matRes = new Resultat(res, res.size());
        return matRes;
    }

    // Retourne une liste contenant les valeurs soustraite MatA et MatB
    public Resultat calculMatriceSub(List<Double> matA, List<Double> matB) {

        Resultat matRes;
        List<Double> res = new ArrayList<>();

        for (int i = 0; i < matA.size(); i++) {

            res.add(matA.get(i) - matB.get(i));
        }

        matRes = new Resultat(res, res.size());
        return matRes;
    }

    // Retourne une liste contenant les valeurs inverse MatA
    public Resultat calculMatriceInvert(List<Double> matA) {

        Resultat matRes;
        List<Double> res = new ArrayList<>();

        if (matA.size() == 4) { // 2x2

            Double tmp;
            tmp = (matA.get(0) * matA.get(3)) - (matA.get(1) * matA.get(2));

            if (tmp != 0) { // Alors matrice a un inverse

                tmp = 1.0 / tmp;

                res.add(tmp * matA.get(3)); // d
                res.add(tmp * (-matA.get(1))); // -b
                res.add(tmp * (-matA.get(2))); // -c
                res.add(tmp * matA.get(0)); // a
            }
        }

        // TODO : Gérer inverse matrice 3x3

        matRes = new Resultat(res, res.size());
        return matRes;
    }
}
