package com.brightrymer.infomath;

/**
 * Created by Bright P.-E. RYMER on 04/11/2015.
 * For InfoMath18
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class UniformActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RelativeLayout rl_main_layout;
    EditText et_a, et_b;
    Button btn_calculate, btn_hide_show;
    TextView tv_result_uniform;
    LineChart lc_uniform_line_chart;
    int id = 0;
    Miscellaneous misc = new Miscellaneous();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uniform);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        rl_main_layout = (RelativeLayout) findViewById(R.id.uniform_container);
        et_a = (EditText) findViewById(R.id.uniform_a_init);
        et_b = (EditText) findViewById(R.id.uniform_b_init);
        tv_result_uniform = (TextView) findViewById(R.id.result_uniform);
        btn_calculate = (Button) findViewById(R.id.uniform_calculate);
        btn_hide_show = (Button) findViewById(R.id.uniform_hide_show);

        lc_uniform_line_chart = (LineChart) findViewById(R.id.uniform_line_chart);

        btn_calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    // On masque les résultats
                    tv_result_uniform.setVisibility(View.GONE);
                    btn_hide_show.setVisibility(View.GONE);
                    lc_uniform_line_chart.setVisibility(View.GONE);
                    lc_uniform_line_chart.invalidate();

                    hideKeyboard();

                    // On calcule et affiche
                    updateResults();
                } catch (Exception e) {

                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_hide_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rl_main_layout.getVisibility() == View.GONE) { // On affiche le Chart

                    rl_main_layout.setVisibility(View.VISIBLE);
                    lc_uniform_line_chart.setVisibility(View.GONE);
                    btn_hide_show.setText(getResources().getString(R.string.show));
                    findViewById(R.id.separator).setVisibility(View.VISIBLE);
                } else { // On cache le Chart

                    rl_main_layout.setVisibility(View.GONE);
                    lc_uniform_line_chart.setVisibility(View.VISIBLE);
                    btn_hide_show.setText(getResources().getString(R.string.hide_show_graphic));
                    findViewById(R.id.separator).setVisibility(View.GONE);
                }
            }
        });
    }

    // Calcul et affiche les résultats de la loi de Binomiale trouvés
    private void updateResults() {

        if (et_a != null && et_b != null && tv_result_uniform != null) {

            int a = Integer.parseInt(et_a.getText().toString());
            int b = Integer.parseInt(et_b.getText().toString());

            if (b < a) {

                et_b.requestFocus();
                throw new IllegalArgumentException("b < a : Impossible.");
            }

            Double result;

            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
            symbols.setDecimalSeparator('.');
            DecimalFormat myDf = new DecimalFormat("#.###", symbols);
            DecimalFormat myDfPercent = new DecimalFormat("#.##", symbols);

            // Stockage des résultats pour construire le graphique par la suite
            LinkedHashMap<Integer, Double> resultXProba = new LinkedHashMap<>();

            // Stockage des résultats pour construire le graphique
            ArrayList<Entry> valeurKProba = new ArrayList<>();
            // Label de l'axe x
            ArrayList<String> labels = new ArrayList<>();

            for (int k = a + (a / 2); (k < b + Math.abs(a) + 1); k++) {

                if ((k < a || k > b)) {

                    result = 0.0;
                } else {

                    result = misc.calculUniform(a, b);
                }
                resultXProba.put(k, result);

                valeurKProba.add(new Entry(Float.valueOf(myDf.format(result)), k));
                labels.add(String.valueOf(k));
            }

            String densityProbability = getResources().getString(R.string.proba_density);
            String uniformTitreBar = "";

            // Remplissage et configuration du BarChart
            LineDataSet myLineDataset = new LineDataSet(valeurKProba, uniformTitreBar);
            myLineDataset.setDrawValues(false);
            myLineDataset.setDrawCircles(false);
            LineData myLineData = new LineData(labels, myLineDataset);

            XAxis myXAxis = lc_uniform_line_chart.getXAxis();

            myXAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            myXAxis.setTextSize(11f);
            myXAxis.setDrawGridLines(false);

            lc_uniform_line_chart.getAxisLeft().setTextSize(11f);

            lc_uniform_line_chart.setData(myLineData);
            lc_uniform_line_chart.setDrawGridBackground(false);
            lc_uniform_line_chart.setDescription("");
            lc_uniform_line_chart.getAxisRight().setEnabled(false); // No right axis
            lc_uniform_line_chart.animateXY(3000, 3000);
            btn_hide_show.setVisibility(View.VISIBLE);

            densityProbability = densityProbability + " " + myDf.format(misc.calculUniform(a, b));
            tv_result_uniform.setText(densityProbability);
            tv_result_uniform.setVisibility(View.VISIBLE);
        }
    }

    // Fonction pour tester si EditText est vide.
    private boolean isEmpty(EditText myeditText) {
        return myeditText.getText().toString().trim().length() == 0;
    }

    private void hideKeyboard() {

        // On cache le clavier
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rl_main_layout.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        id = item.getItemId();

        if (id == R.id.nav_sec_degree) {
            // Handle the second degree action
            try {
                Intent intent = new Intent(this, SecDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_third_degree) {
            // Handle the third degree action
            try {
                Intent intent = new Intent(this, ThirDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_stat) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, StatisticActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_unif_distrib) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, UniformActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_binom_distrib) {
            // Handle the binomial distribution action
            try {
                Intent intent = new Intent(this, BinomialActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_poiss_distrib) {
            // Handle the poisson distribution action
            try {
                Intent intent = new Intent(this, PoissonActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_norma_distrib) {
            // Handle the normal distribution action
            try {
                Intent intent = new Intent(this, NormalActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_test_khi) {

        } else if (id == R.id.nav_matrix) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, MatrixActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.license) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, LicenseActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
