package com.brightrymer.infomath;

/**
 * Created by Bright P.-E. RYMER on 04/11/2015.
 * For InfoMath18
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class BinomialActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RelativeLayout rl_main_layout;
    EditText et_n, et_p, et_range_min, et_range_max, et_occurence;
    Button btn_calculate, btn_hide_show;
    TextView tv_result_range, tv_result_exact;
    BarChart bc_binomial_bar_chart;
    int id = 0;
    Miscellaneous misc = new Miscellaneous();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binomial);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        rl_main_layout = (RelativeLayout) findViewById(R.id.binomial_container);
        et_n = (EditText) findViewById(R.id.n_init);
        et_p = (EditText) findViewById(R.id.p_init);
        et_range_min = (EditText) findViewById(R.id.borne_min);
        et_range_max = (EditText) findViewById(R.id.borne_max);
        et_occurence = (EditText) findViewById(R.id.exact_occ);
        tv_result_range = (TextView) findViewById(R.id.result_uniform);
        tv_result_exact = (TextView) findViewById(R.id.result_exact);
        btn_calculate = (Button) findViewById(R.id.binomial_calculate);
        btn_hide_show = (Button) findViewById(R.id.binomial_hide_show);

        bc_binomial_bar_chart = (BarChart) findViewById(R.id.binomial_bar_chart);

        btn_calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    // On masque les résultats
                    tv_result_exact.setVisibility(View.GONE);
                    tv_result_range.setVisibility(View.GONE);
                    btn_hide_show.setVisibility(View.GONE);
                    bc_binomial_bar_chart.setVisibility(View.GONE);
                    bc_binomial_bar_chart.invalidate();

                    hideKeyboard();

                    // On calcule et affiche
                    updateResults();
                } catch (Exception e) {

                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_hide_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rl_main_layout.getVisibility() == View.GONE) { // On affiche le Chart

                    rl_main_layout.setVisibility(View.VISIBLE);
                    bc_binomial_bar_chart.setVisibility(View.GONE);
                    btn_hide_show.setText(getResources().getString(R.string.show));
                    findViewById(R.id.separator).setVisibility(View.VISIBLE);
                } else { // On cache le Chart

                    rl_main_layout.setVisibility(View.GONE);
                    bc_binomial_bar_chart.setVisibility(View.VISIBLE);
                    btn_hide_show.setText(getResources().getString(R.string.hide_show_graphic));
                    findViewById(R.id.separator).setVisibility(View.GONE);
                }
            }
        });
    }

    // Calcul et affiche les résultats de la loi de Binomiale trouvés
    private void updateResults() {

        if (et_n != null && et_p != null && et_range_min != null && et_range_max != null && tv_result_range != null && et_occurence != null) {

            int n = Integer.parseInt(et_n.getText().toString());

            if (n > 1000 || n < 1) {

                throw new IllegalArgumentException("n " + getString(R.string.err_not_within_params));
            }

            Double p = Double.parseDouble(et_p.getText().toString());

            if (p < 0 || p > 1) {

                throw new IllegalArgumentException("p " + getString(R.string.err_not_within_params));
            }

            int borneMin = 0;
            int borneMax = 0;
            int occ = 0;
            int limit = n;
            boolean doExact = false, doRange = false;
            Double result = 0.0, resultKUser = 0.0;

            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
            symbols.setDecimalSeparator('.');
            DecimalFormat myDf = new DecimalFormat("#.#######", symbols);
            DecimalFormat myDfPercent = new DecimalFormat("#.##", symbols);

            if (!isEmpty(et_occurence)) { // Si l'utilisateur a renseigné à la case et_occurence

                occ = Integer.parseInt(et_occurence.getText().toString());
                doExact = true;
            }

            if ((!isEmpty(et_range_min)) && (!isEmpty(et_range_max))) { // Si l'utilisateur a renseigné la case min et max

                borneMin = Integer.parseInt(et_range_min.getText().toString());
                borneMax = Integer.parseInt(et_range_max.getText().toString());
                doRange = true;

                if (borneMax > n * 2) {

                    limit = borneMax;
                }
            }

            // Stockage des résultats pour construire le graphique par la suite
            LinkedHashMap<Integer, Double> resultXProba = new LinkedHashMap<>();

            // Stockage des résultats pour construire le graphique
            ArrayList<BarEntry> valeurKProba = new ArrayList<>();
            // Label de l'axe x
            ArrayList<String> labels = new ArrayList<>();

            for (int k = 0; k <= limit; k++) { // Pour chaque valeur de P(X = k), on calcule la probabilité.

                result = misc.calculBinomial(n, p, k).doubleValue();
                resultXProba.put(k, result);

                valeurKProba.add(new BarEntry(Float.valueOf(myDf.format(result)), k));
                labels.add(String.valueOf(k));

                if (doExact && k == occ) {

                    resultKUser = result;
                }
            }

            if (doRange) { // On cherche la probabilité de l'intervalle fourni par l'utilisateur

                result = misc.probaInRange(resultXProba, borneMin, borneMax);
            }

            String binomialLineInit = getResources().getString(R.string.borne_init_poisson);
            String binomialExact = getResources().getString(R.string.poisson_exact_result);
            String binomialTitreBar = getResources().getString(R.string.poisson_barchart_title);
            String and = getResources().getString(R.string.and);
            String is = getResources().getString(R.string.is);

            // Remplissage et configuration du BarChart
            BarDataSet myBarDataset = new BarDataSet(valeurKProba, binomialTitreBar);
            BarData myBarData = new BarData(labels, myBarDataset);

            XAxis myXAxis = bc_binomial_bar_chart.getXAxis();
            myXAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            myXAxis.setTextSize(11f);
            myXAxis.setDrawGridLines(false);

            bc_binomial_bar_chart.getAxisLeft().setTextSize(11f);

            bc_binomial_bar_chart.setData(myBarData);
            bc_binomial_bar_chart.setDrawValueAboveBar(true);
            bc_binomial_bar_chart.setDrawGridBackground(false);
            bc_binomial_bar_chart.setDescription("");
            bc_binomial_bar_chart.getAxisRight().setEnabled(false); // No right axis
            bc_binomial_bar_chart.animateXY(3000, 3000);

            if (doRange) {

                String res = binomialLineInit + " " + borneMin + " " + and + " " + borneMax + " " + is + " " + myDf.format(result) + " (" + myDfPercent.format(result * 100) + "%).";
                tv_result_range.setText(res);
                tv_result_range.setVisibility(View.VISIBLE);
            }
            btn_hide_show.setVisibility(View.VISIBLE);
            if (doExact) {

                String res = binomialExact + " " + occ + " " + getResources().getString(R.string.occurence) + " " + is + " " + myDf.format(resultKUser) + " (" + myDfPercent.format(resultKUser * 100) + "%).";
                tv_result_exact.setText(res);
                tv_result_exact.setVisibility(View.VISIBLE);
            }
        }
    }

    // Fonction pour tester si EditText est vide.
    private boolean isEmpty(EditText myeditText) {
        return myeditText.getText().toString().trim().length() == 0;
    }

    private void hideKeyboard() {

        // On cache le clavier
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rl_main_layout.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        id = item.getItemId();

        if (id == R.id.nav_sec_degree) {
            // Handle the second degree action
            try {
                Intent intent = new Intent(this, SecDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_third_degree) {
            // Handle the third degree action
            try {
                Intent intent = new Intent(this, ThirDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_stat) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, StatisticActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_unif_distrib) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, UniformActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_binom_distrib) {
            // Handle the binomial distribution action
            try {
                Intent intent = new Intent(this, BinomialActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_poiss_distrib) {
            // Handle the poisson distribution action
            try {
                Intent intent = new Intent(this, PoissonActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_norma_distrib) {
            // Handle the normal distribution action
            try {
                Intent intent = new Intent(this, NormalActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_test_khi) {

        } else if (id == R.id.nav_matrix) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, MatrixActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.license) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, LicenseActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
