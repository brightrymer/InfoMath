package com.brightrymer.infomath;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class SecDegActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RelativeLayout rl_main_layout;
    Button btn_sec_deg;
    EditText et_a, et_b, et_c;
    int id = 0;
    Miscellaneous misc = new Miscellaneous();
    String res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sec_deg);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        rl_main_layout = (RelativeLayout) findViewById(R.id.equation_line);

        btn_sec_deg = (Button) findViewById(R.id.sec_deg_calc);

        et_a = (EditText) findViewById(R.id.edittext_a);
        et_b = (EditText) findViewById(R.id.edittext_b);
        et_c = (EditText) findViewById(R.id.edittext_c);

        btn_sec_deg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    double val_a, val_b, val_c;

                    DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
                    symbols.setDecimalSeparator('.');
                    DecimalFormat myDoubleFormat = new DecimalFormat("#.##", symbols);

                    val_a = Double.parseDouble(et_a.getText().toString());
                    val_b = Double.parseDouble(et_b.getText().toString());
                    val_c = Double.parseDouble(et_c.getText().toString());

                    Resultat resCalcul = misc.secondDegree(val_a, val_b, val_c);

                    TextView discr = (TextView) findViewById(R.id.discriminant_result);
                    TextView discr_sens = (TextView) findViewById(R.id.discriminant_sens);
                    TextView equat_sol_x1 = (TextView) findViewById(R.id.equation_solution_x1);
                    TextView equat_sol_x2 = (TextView) findViewById(R.id.equation_solution_x2);

                    res = " " + myDoubleFormat.format(resCalcul.getDiscriminant());
                    discr.setText(res);
                    equat_sol_x1.setText("");
                    equat_sol_x2.setText("");

                    hideKeyboard();
                    findViewById(R.id.discriminant).setVisibility(View.VISIBLE);

                    if (resCalcul.getType().equals("Sec Deg 2")) {

                        res = getResources().getString(R.string.discriminant_sign) + " > 0 : Deux solutions.";
                        discr_sens.setText(res);

                        res = "x1 = " + myDoubleFormat.format(resCalcul.getX1()) + ".";
                        equat_sol_x1.setText(res);

                        res = "x2 = " + myDoubleFormat.format(resCalcul.getX2()) + ".";
                        equat_sol_x2.setText(res);
                    } else {
                        if (resCalcul.getType().equals("Sec Deg 1")) {

                            res = getResources().getString(R.string.discriminant_sign) + " = 0 : Une solution.";
                            discr_sens.setText(res);

                            res = "x1 = " + myDoubleFormat.format(resCalcul.getX1()) + ".";
                            equat_sol_x1.setText(res);
                        } else {

                            res = getResources().getString(R.string.discriminant_sign) + " < 0 : Deux solutions complexes.";
                            discr_sens.setText(res);

                            res = "x1 = " + myDoubleFormat.format(resCalcul.getX1()) + " + i" + myDoubleFormat.format(Math.abs(resCalcul.getX2()));
                            equat_sol_x1.setText(res);

                            res = "x2 = " + myDoubleFormat.format(resCalcul.getX1()) + " - i" + myDoubleFormat.format(Math.abs(resCalcul.getX2()));
                            equat_sol_x2.setText(res);
                        }
                    }
                } catch (Exception e) {

                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void hideKeyboard() {

        // On cache le clavier
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rl_main_layout.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        id = item.getItemId();

        if (id == R.id.nav_sec_degree) {
            // Handle the second degree action
            try {
                Intent intent = new Intent(this, SecDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_third_degree) {
            // Handle the third degree action
            try {
                Intent intent = new Intent(this, ThirDegActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_stat) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, StatisticActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_unif_distrib) {
            // Handle the statistic action
            try {
                Intent intent = new Intent(this, UniformActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_binom_distrib) {
            // Handle the binomial distribution action
            try {
                Intent intent = new Intent(this, BinomialActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_poiss_distrib) {
            // Handle the poisson distribution action
            try {
                Intent intent = new Intent(this, PoissonActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_norma_distrib) {
            // Handle the normal distribution action
            try {
                Intent intent = new Intent(this, NormalActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_test_khi) {

        } else if (id == R.id.nav_matrix) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, MatrixActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.license) {
            // Handle the matrix action
            try {
                Intent intent = new Intent(this, LicenseActivity.class);

                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
