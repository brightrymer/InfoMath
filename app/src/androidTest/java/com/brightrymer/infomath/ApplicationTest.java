package com.brightrymer.infomath;

import android.app.Application;

import junit.framework.TestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends TestCase {
    public ApplicationTest() {
        super(String.valueOf(Application.class));
    }
}