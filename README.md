Android-oriented only, InfoMath' is an app made in order to provide a simple product to compute or verify your calculus.

It allows you to process second and third degree, statistics, matrix and some probability distribution computing.

Only for Android 4.0.3+